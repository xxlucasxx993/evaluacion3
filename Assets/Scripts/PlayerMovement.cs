using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public int vidaInicial = 3; 
    public float velocidad = 5f;
    public GameObject proyectilPrefab; 
    public float tiempoDestruccionProyectil = 5f; 

    private int vidaActual; 
    private Rigidbody2D rb; 

    void Start()
    {
        vidaActual = vidaInicial;

        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        Vector3 movimiento = new Vector3(movimientoHorizontal, 0f, 0f);
        transform.Translate(movimiento * velocidad * Time.deltaTime);

        MantenerDentroDeLimites();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Disparar();
        }
    }

    void Disparar()
    {
        GameObject proyectil = Instantiate(proyectilPrefab, transform.position, Quaternion.identity);
        
        Destroy(proyectil, tiempoDestruccionProyectil);
    }

    void MantenerDentroDeLimites()
    {
        Vector3 posicionVista = Camera.main.WorldToViewportPoint(transform.position);

        posicionVista.x = Mathf.Clamp(posicionVista.x, 0f, 1f);
        posicionVista.y = Mathf.Clamp(posicionVista.y, 0f, 1f);

        transform.position = Camera.main.ViewportToWorldPoint(posicionVista);
    }

    public void RecibirDanio(int cantidad)
    {
        vidaActual -= cantidad;

        if (vidaActual <= 0)
        {
            vidaActual = 0; 
            Debug.Log("¡Jugador destruido!");
            ReiniciarEscena();
        }
    }

    void ReiniciarEscena()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}