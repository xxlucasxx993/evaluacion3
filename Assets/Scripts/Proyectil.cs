using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{
    public float velocidad = 10f;
    public int damage = 1; 
    public float tiempoVida = 5f; 

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.up * velocidad;

        Destroy(gameObject, tiempoVida);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemigo"))
        {
            Enemigo enemigo = other.GetComponent<Enemigo>();
            if (enemigo != null)
            {
                enemigo.RecibirDanio(damage); 
            }
            
            Enemigo2 enemigo2 = other.GetComponent<Enemigo2>();
            if (enemigo2 != null)
            {
                enemigo2.RecibirDanio(damage);
            }

            Destroy(gameObject);
        }
        else if (other.CompareTag("Laser"))
        {
            CompararYDestruir(other);
        }
    }

    void CompararYDestruir(Collider2D other)
    {
        Vector2 direccionProyectil = GetComponent<Rigidbody2D>().velocity.normalized;
        Vector2 direccionLaser = other.GetComponent<Rigidbody2D>().velocity.normalized;

        float angulo = Vector2.Angle(direccionProyectil, direccionLaser);

        if (angulo > 170f)
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}