using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorEnemigos : MonoBehaviour
{
    public GameObject enemigoPrefab; 
    public float tiempoCreacionEnemigo = 5f; 
    public float velocidadDescenso = 2f; 

    void Start()
    {
        StartCoroutine(CrearEnemigos());
    }

    IEnumerator CrearEnemigos()
    {
        while (true)
        {
            float posX = Random.Range(Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x, Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x);
            Vector3 posicionInicial = new Vector3(posX, Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).y, 0f);
            
            GameObject nuevoEnemigo = Instantiate(enemigoPrefab, posicionInicial, Quaternion.identity);

            StartCoroutine(MoverEnemigo(nuevoEnemigo));

            yield return new WaitForSeconds(tiempoCreacionEnemigo);
        }
    }

    IEnumerator MoverEnemigo(GameObject enemigo)
    {
        while (enemigo.transform.position.y > 0)
        {
            enemigo.transform.Translate(Vector3.down * velocidadDescenso * Time.deltaTime);
            yield return null;
        }
    }
}
