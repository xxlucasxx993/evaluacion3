using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public float velocidad = 10f; 
    public int damage = 1; 
    public float tiempoVida = 5f; 

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.down * velocidad;

        Destroy(gameObject, tiempoVida);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerMovement player = other.GetComponent<PlayerMovement>();
            if (player != null)
            {
                player.RecibirDanio(damage); 
            }
            Destroy(gameObject);
        }
        else if (other.CompareTag("Proyectil"))
        {
            CompararYDestruir(other);
        }
    }

    void CompararYDestruir(Collider2D other)
    {
        Vector2 direccionLaser = GetComponent<Rigidbody2D>().velocity.normalized;
        Vector2 direccionProyectil = other.GetComponent<Rigidbody2D>().velocity.normalized;

        float angulo = Vector2.Angle(direccionLaser, direccionProyectil);

        if (angulo > 170f)
        {
            Destroy(other.gameObject); 
            Destroy(gameObject); 
        }
    }
}