using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2 : MonoBehaviour
{
    public float velocidadMovimiento = 2f; 
    public float limiteDerecho = 16f; 
    public float limiteIzquierdo = -16f; 
    public int vida = 1; 
    public GameObject laserPrefab; 
    public float tiempoDestruccionProyectil = 5f; 
    public float tiempoEsperaRespawn = 3f; 
    public float probabilidadAparicionIzquierda = 0.5f; 

    private Vector3 posicionInicial; 
    private Coroutine disparoCoroutine; 

    void Start()
    {
        posicionInicial = transform.position; 
        disparoCoroutine = StartCoroutine(DispararLaser()); 
    }

    void Update()
    {
        float movimientoHorizontal = velocidadMovimiento * Time.deltaTime;
        transform.Translate(Vector3.right * movimientoHorizontal);

        if (transform.position.x >= limiteDerecho || transform.position.x <= limiteIzquierdo)
        {
            CambiarDireccion();
        }
    }

    IEnumerator DispararLaser()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(1f, 3f));

            GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity);

            laser.GetComponent<Rigidbody2D>().velocity = Vector2.down * 5f; 

            Destroy(laser, tiempoDestruccionProyectil);
        }
    }

    public void RecibirDanio(int cantidad)
    {
        vida -= cantidad;

        if (vida <= 0)
        {
            Respawn();
        }
    }

    void Respawn()
    {
        vida = 1; 

        float randomValue = Random.value;
        Vector3 respawnPosition = randomValue < probabilidadAparicionIzquierda ?
                                  new Vector3(limiteIzquierdo, posicionInicial.y, posicionInicial.z) :
                                  new Vector3(limiteDerecho, posicionInicial.y, posicionInicial.z);
        transform.position = respawnPosition;

        if (disparoCoroutine != null)
        {
            StopCoroutine(disparoCoroutine);
        }

        disparoCoroutine = StartCoroutine(DispararLaser());
    }

    void CambiarDireccion()
    {
        velocidadMovimiento *= -1f;
        Flip(); 
    }

    void Flip()
    {
        Vector3 escala = transform.localScale;
        escala.x *= -1;
        transform.localScale = escala;
    }
}