using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public float velocidadMovimiento = 2f; 
    public float distanciaPatrulla = 4f; 
    private Vector3 posicionInicial; 
    private int direccion = 1;
    public int vida = 1;
    public GameObject laserPrefab; 

    void Start()
    {
        posicionInicial = transform.position;
        StartCoroutine(DispararLaser());
    }

    void Update()
    {
        float movimientoHorizontal = direccion * velocidadMovimiento * Time.deltaTime;
        transform.Translate(new Vector3(movimientoHorizontal, 0, 0));

        if (Mathf.Abs(transform.position.x - posicionInicial.x) >= distanciaPatrulla / 2)
        {
            CambiarDireccion();
        }
    }

    void CambiarDireccion()
    {
        direccion *= -1;
        Flip();
    }

    void Flip()
    {
        Vector3 escala = transform.localScale;
        escala.x *= -1;
        transform.localScale = escala;
    }

    IEnumerator DispararLaser()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(1f, 3f));

            GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity);

            laser.GetComponent<Rigidbody2D>().velocity = Vector2.down * 5f;
        }
    }

    public void RecibirDanio(int cantidad)
    {
        vida -= cantidad;

        if (vida <= 0)
        {
            Destruir();
        }
    }

    void Destruir()
    {
        Destroy(gameObject);
    }
}